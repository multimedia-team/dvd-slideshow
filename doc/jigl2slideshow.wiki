Content-type: text/html; charset=UTF-8

Man page of jigl2slideshow

==jigl2slideshow==
Section: dvd-slideshow (1)Updated: 0.4[Index]
[Return to Main Contents]
&nbsp;
===NAME===

jigl2slideshow - Creates an input file for dvd-slideshow from pictures in a jigl online photo album.
&nbsp;
===SYNOPSIS===

jigl2slideshow '''-o''' &lt;'''output directory'''&gt; ['''-t''' &lt;'''Seconds per picture'''&gt;] &lt;'''path to album'''&gt;
&nbsp;
===DESCRIPTION===

Generates a text file listing of the pictures visible in a given jigl ([http://xome.net/projects/jigl/)] photo album in order to easily pass the information to dvd_slideshow.

This has not been updated for some time, so if you use this, let the developers know so they know there is interest...
&nbsp;
===OPTIONS===


['''-o''' &lt;'''Output directory'''&gt;]
Directory where the output file will be written.  If not specified, the current directory is used.

['''-t''' &lt;'''Seconds per picture'''&gt;]
Number of seconds to display each picture in the movie.  Defaults to 5 seconds if not specified.

&lt;'''path to album'''&gt;
The path to the gallery.dat that you want to generate a slideshow for.

&nbsp;
===EXAMPLES===

&nbsp;
===AUTHORS===

Scott Dylewski &lt;scott at dylewski dot com&gt;

[http://dvd-slideshow.sourceforge.net]
&nbsp;
===SEE ALSO===

[dvd-slideshow](1)


&nbsp;===Index===

[NAME]
[SYNOPSIS]
[DESCRIPTION]
[OPTIONS]
[EXAMPLES]
[AUTHORS]
[SEE ALSO]


This document was created by
[man2html],
using the manual pages.
Time: 20:08:45 GMT, December 30, 2015


